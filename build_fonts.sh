#!/bin/bash

set -e

cd /repos/hack
make
cp -a build/* /code/fonts/
