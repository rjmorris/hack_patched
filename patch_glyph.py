import argparse
import logging
from pathlib import Path
import re
import shutil


logging.basicConfig(
    style="{",
    format="{levelname}: {message}",
    level=logging.DEBUG,
)


styles = [
    "Regular",
    "Bold",
    "Italic",
    "BoldItalic",
]


hint_names = {
    "0021": "exclam",
    "0023": "numbersign",
    "0025": "percent",
    "002B": "plus",
    "0030": "zero",
    "0038": "eight",
}


def patch_glyph(name):
    """
    Patch the Hack font files to use the alt_hack glyph of the specified name.

    The steps to patch the font include:
      - Overwrite Hack's glyph files with alt_hack's.
      - Remove the original glyph's hints from the hinting instructions file.
    """

    for style in styles:
        logging.info(f"Overwriting original glyphs for {name}, {style}")
        overwrite_glyph(
            from_dir=get_alt_hack_glyph_dir(name, style),
            to_dir=get_hack_glyph_dir(style),
        )

    codepoint = get_codepoint(name)
    if codepoint in hint_names:
        for style in styles:
            logging.info(f"Removing hint instructions for {name}, {style}")
            remove_hints(hint_names[codepoint], get_instructions_file(style))


def overwrite_glyph(from_dir, to_dir):
    """
    Copy all the glyph files from one directory to another.
    """
    for f in from_dir.glob("*.glif"):
        shutil.copy(f, to_dir)


def get_codepoint(alt_hack_name):
    """
    Extract the Unicode codepoint from the glyph name used by alt_hack.

    In alt_hack, the names look like "u003C-wider". The codepoint extracted and
    returned by this function would be "003C".
    """

    match = re.match("u([0-9A-F]+)-.*", alt_hack_name)
    if match:
        return match.group(1)
    raise RuntimeError(f"Unable to find a codepoint in {alt_hack_name}")


def remove_hints(name, file):
    """
    Rewrite the given hinting instructions file by commenting out any lines for
    the specified hint name.
    """

    # This is a really small file, so it's OK to read the whole thing into
    # memory.

    with file.open() as f:
        lines = f.readlines()

    with file.open(mode="w") as f:
        f.writelines(comment_startswith(lines, name))


def comment_startswith(strings, prefix, comment="# "):
    """
    Yield a revised version of strings, where the comment string has been
    prepended to each string that starts with the given prefix.
    """
    for s in strings:
        if s.lstrip().startswith(prefix):
            yield f"{comment}{s}"
        else:
            yield s


def get_alt_hack_glyph_dir(name, style):
    """
    Return the path in the alt_hack repo to the directory containing glyphs for
    the specified glyph name and font style.
    """
    return Path(f"/repos/alt_hack/glyphs/{name}/{style.lower()}")


def get_hack_glyph_dir(style):
    """
    Return the path in the hack repo to the directory containing glyphs for
    the specified font style.
    """
    return Path(f"/repos/hack/source/Hack-{style}.ufo/glyphs")


def get_instructions_file(style):
    """
    Return the path to the hinting instructions file for the given font style.
    """
    return Path(f"/repos/hack/postbuild_processing/tt-hinting/Hack-{style}-TA.txt")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("glyphs", nargs="*")
    args = parser.parse_args()

    for glyph in args.glyphs:
        patch_glyph(glyph)
