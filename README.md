Build a patched version of the [Hack font](https://github.com/source-foundry/Hack) using glyphs from the [alt-hack repository](https://github.com/source-foundry/alt-hack).

Dependencies for patching and building are provided using docker and docker-compose.

The scripts provided here are based on specific commits from the source repositories, namely [db2d3fe](https://github.com/source-foundry/Hack/tree/db2d3feb771762953d571cfd3c2b76499f26c23d) for Hack and  [9fab732](https://github.com/source-foundry/alt-hack/tree/9fab7328cfc208046cb7f3d713ec97a724d8219a) for alt-hack. The Hack commit is from its dev branch, chosen to work around a bug in the master branch (see comments in [Dockerfile](./Dockerfile) for details). The alt-hack commit is the latest one at the time of creating this repository.

# Workflow

## Step 1: Build the docker image

The docker image will contain the hack and alt-hack repositories as well as the dependencies needed to build the fonts. Build the image with:

```
$ docker-compose build
```

## Step 2: Patch Hack with alternative glyphs

The alt-hack repository contains an alternative version of several individual Hack glyphs. You can use as many of them as you like.

Screenshots of the alternative glyphs can be found in the `README` files in the subdirectories of alt-hack's [glyphs/ directory]( https://github.com/source-foundry/alt-hack/tree/9fab7328cfc208046cb7f3d713ec97a724d8219a/glyphs). Make note of the subdirectory names of alternative glyphs you want to use. For example, if you like the forward slashed zero, the subdirectory name is `u0030-forwardslash`.

This repository provides a script for patching the Hack font with alternative glyphs. Suppose the alternative glyphs you want to use are `u0028-curved`, `u0029-curved`, `u0030-forwardslash`, and `u0031-noslab`. Then run the patch script as:

```
$ docker-compose run --rm font python patch_glyph.py u0028-curved u0029-curved u0030-forwardslash u0031-noslab
```

## Step 3: Build the modified Hack font

This repository provides a script for building the Hack font. Run the script with:

```
$ docker-compose run --rm font ./build_fonts.sh
```

## Step 4: Install the modified Hack font

The font files that were created in the previous step will be located in the [./fonts](./fonts) directory. The method for installing the fonts is system dependent. For example, on Linux you might install the ttf fonts with:

```
$ mkdir ~/.local/share/fonts/HackPatched
$ cp ./fonts/ttf/* ~/.local/share/fonts/HackPatched
$ fc-cache
```

If you already had a Hack font installed, first you should probably delete/uninstall it and run `fc-cache` to clear your old Hack version from the font cache, and then run the above commands to install the new version.

# Tests

This repository contains unit tests for the Python script that copies files from alt-hack to Hack. Run these tests with `docker-compose run --rm font pytest`.
