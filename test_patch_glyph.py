from pathlib import Path
import pytest

from patch_glyph import (
    comment_startswith,
    get_codepoint,
    get_alt_hack_glyph_dir,
    get_hack_glyph_dir,
    get_instructions_file,
    overwrite_glyph,
    remove_hints,
    styles,
)


@pytest.mark.parametrize("name, codepoint", [
    ("u0030-forwardslash", "0030"),
    ("u003C-wider", "003C"),
])
def test_get_codepoint(name, codepoint):
    assert get_codepoint(name) == codepoint


@pytest.mark.parametrize("name", [
    "u0030",
    "u003c-wider",
    "U+002B-plus",
])
def test_get_codepoint_failure(name):
    with pytest.raises(RuntimeError):
        get_codepoint(name)


@pytest.mark.parametrize("style, path", [
    ("Regular", "/repos/hack/postbuild_processing/tt-hinting/Hack-Regular-TA.txt"),
    ("BoldItalic", "/repos/hack/postbuild_processing/tt-hinting/Hack-BoldItalic-TA.txt"),
])
def test_get_instructions_file(style, path):
    assert get_instructions_file(style) == Path(path)


@pytest.mark.parametrize("style, path", [
    ("Bold", "/repos/hack/source/Hack-Bold.ufo/glyphs"),
    ("Italic", "/repos/hack/source/Hack-Italic.ufo/glyphs"),
])
def test_get_hack_glyph_dir(style, path):
    assert get_hack_glyph_dir(style) == Path(path)


@pytest.mark.parametrize("name, style, path", [
    ("u012B-slab", "Regular", "/repos/alt_hack/glyphs/u012B-slab/regular"),
    ("u0030-forwardslash", "Bold", "/repos/alt_hack/glyphs/u0030-forwardslash/bold"),
    ("u0029-curved", "Italic", "/repos/alt_hack/glyphs/u0029-curved/italic"),
    ("u003E-wider", "BoldItalic", "/repos/alt_hack/glyphs/u003E-wider/bolditalic"),
])
def test_get_hack_glyph_dir(name, style, path):
    assert get_alt_hack_glyph_dir(name, style) == Path(path)


@pytest.fixture
def orig_hints():
    return [
        "",
        "# zero",
        "zero",
        "zero a 1.5 b 2",
        "  zero",
        " zero  ",
        " zero a 1.5 b 2",
        "plus",
        " plus a 1.5 b 2",
    ]


@pytest.fixture
def commented_hints():
    return [
        "",
        "# zero",
        "# zero",
        "# zero a 1.5 b 2",
        "#   zero",
        "#  zero  ",
        "#  zero a 1.5 b 2",
        "plus",
        " plus a 1.5 b 2",
    ]


def test_comment_startswith(orig_hints, commented_hints):
    observed = list(comment_startswith(orig_hints, "zero"))
    assert observed == commented_hints


def test_remove_hints(tmpdir, orig_hints, commented_hints):
    hints_file = Path(tmpdir) / "hints.txt"
    with hints_file.open(mode="w") as f:
        f.writelines([f"{hint}\n" for hint in orig_hints])

    remove_hints("zero", hints_file)

    with hints_file.open() as f:
        observed = f.readlines()

    expected = [f"{hint}\n" for hint in commented_hints]

    assert observed == expected


def create_file(path, contents):
    path.parent.mkdir(exist_ok=True, parents=True)
    with path.open(mode="w") as f:
        f.write(contents)


def read_file(path):
    with path.open() as f:
        return f.read()


def test_overwrite_glyph(tmp_path):
    new_dir = tmp_path / "new"
    old_dir = tmp_path / "old"

    new_files = {
        "aaa.glif": "new_aaa",
        "bbb.md": "new_bbb",
        "ccc.glif": "new_ccc",
        "ddd/ddd.glif": "new_ddd",
        "ddd/eee.md": "new_eee",
    }

    old_files = {
        "aaa.glif": "old_aaa",
        "bbb.md": "old_bbb",
        "fff.glif": "old_fff",
        "ddd/ddd.glif": "old_ddd",
        "ddd/eee.md": "old_eee",
    }

    for name in new_files:
        create_file(new_dir / name, new_files[name])
    for name in old_files:
        create_file(old_dir / name, old_files[name])

    overwrite_glyph(new_dir, old_dir)

    for name in new_files:
        new_path = new_dir / name
        is_glif = new_path.suffix == ".glif"
        is_in_subdir = new_path.parent != new_dir

        old_path = old_dir / name

        if is_glif and not is_in_subdir:
            assert read_file(old_path) == new_files[name]
        else:
            if name in old_files:
                assert read_file(old_path) == old_files[name]
            else:
                assert(not old_path.exists())

    for name in old_files:
        if name not in new_files:
            old_path = old_dir / name
            assert read_file(old_path) == old_files[name]
