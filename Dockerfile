FROM python:3.7.5-buster@sha256:f0db6711abee8d406121c9e057bc0f7605336e8148006164fea2c43809fe7977

WORKDIR /repos

# At the time of writing, the latest Hack release was tagged v3.003. Instead of
# checking out that release, however, I'm checking out a commit from the dev
# branch. This is because the release version won't compile using current
# versions of the dependencies, and it isn't clear which older versions of the
# dependencies should work. This problem has been fixed in dev but hasn't been
# rolled into an official release yet.
#
# See the following issues for more discussion:
#
# - https://github.com/source-foundry/Hack/issues/481
# - https://github.com/ryanoasis/nerd-fonts/issues/70

RUN git clone https://github.com/source-foundry/Hack.git hack \
    && cd hack \
    && git checkout db2d3fe

# Check out the latest commit from alt-hack at the time of writing.

RUN git clone https://github.com/source-foundry/alt-hack.git alt_hack \
    && cd alt_hack \
    && git checkout 9fab732

# Compile the Hack dependencies using the scripts shipped with Hack.

WORKDIR /repos/hack
RUN make compile-local-dep

# Install the Python dependencies.

WORKDIR /code

COPY requirements.txt .
RUN pip install --upgrade pip \
    && pip install -r requirements.txt

WORKDIR /repos/hack

RUN pipenv sync
